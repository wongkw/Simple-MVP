# Simple MVP Framework
### &copy; 2015 Wong Kean Wah. Licensed under Creative Commons Attribution License (https://creativecommons.org/licenses/by/4.0/)

## Introduction
Simple-MVP is a simple Model-View-Presenter framework for Android designed for ease of use and extensibility. 

The framework is split into 5 packages:


    -base: Contains the base classes to be extended by any activities that use this framework.
    -helper: Contains classes that aid in the functionality of the framework (for example, the Retained Fragment Helper).
    -model: Contains classes in the Model layer.
    -presenter: Contains classes in the Presenter layer.
    -view: Contains classes in the View layer. (usually Activities)
    
This framework is a work in progress.

Inspired by the Model-View-Presenter framework used in the Coursera POSA MOOCs by Vanderbilt University.
    
