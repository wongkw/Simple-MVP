package kw.kwstudios.simplemvp.helper;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.util.Log;

import java.lang.ref.WeakReference;
import java.util.HashMap;

/**
 * Created by KW on 19/12/2015.
 *
 * Class used to retain state during runtime configuration changes.
 */
public class RetainedFragmentHelper {
    private final String TAG = getClass().getSimpleName();

    //A String used to identify the Retained Fragment
    private final String retainedFragmentId;

    private final WeakReference<FragmentManager> fragmentManagerWeakReference;

    private RetainedFragment retainedFragment;

    public RetainedFragmentHelper(String retainedFragmentId, Activity activity) {
        fragmentManagerWeakReference = new WeakReference<>(activity.getFragmentManager());

        this.retainedFragmentId = retainedFragmentId;
    }

    public boolean retainedFragmentExists() {
        retainedFragment = (RetainedFragment)
                getFragmentManager().findFragmentByTag(retainedFragmentId);
        try {
            if (retainedFragment != null)
                return false;
            else {
                retainedFragment = new RetainedFragment();
                getFragmentManager().beginTransaction().add(retainedFragment, retainedFragmentId)
                        .commit();

                return true;
            }
        }
        catch (NullPointerException e) {
            Log.wtf(TAG, "Null pointer exception in retainedFragmentExists", e);
            return false;
        }

    }

    public void putObject(String key, Object object) {
        retainedFragment.addData(key, object);
    }

    public void putObject(Object object) {
        retainedFragment.addData(object.getClass().getSimpleName(), object);
    }

    public Object getObject(String key) {
        return retainedFragment.getData(key);
    }

    public Object getObject (Object object) {
        return retainedFragment.getData(object.getClass().getSimpleName());
    }

    private FragmentManager getFragmentManager() {
        return fragmentManagerWeakReference.get();
    }



    public static class RetainedFragment extends Fragment {
        //Store data to be retained in a hashmap
        private HashMap<String, Object> mData;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setRetainInstance(true);
        }

        public void addData(String key, Object object) {
            mData.put(key, object);
        }

        public HashMap<String, Object> getData() {
            return mData;
        }

        public Object getData(String id) {
            return mData.get(id);
        }
    }

    }




