package kw.kwstudios.simplemvp.helper;

import android.content.Context;

/**
 * Created by KW on 19/12/2015.
 *
 * This file defines the public-facing
 * methods which classes in the SimpleMVP framework must implement.
 *
 */
public interface MVP {
    interface Model {
        Presenter getPresenter();
    }

    interface View {
        void notifyBeingDestroyed();
        void initializeRetainedFragment(Presenter presenter);
        void reinitializeRetainedFragment(Presenter presenter);
        Context getContext();
    }

    interface Presenter {
        Context getContext();
        boolean isRunning();
        void notifyConfigChanged(MVP.View v);
    }
}
