package kw.kwstudios.simplemvp.base;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import java.lang.ref.WeakReference;

import kw.kwstudios.simplemvp.helper.MVP;
import kw.kwstudios.simplemvp.helper.RetainedFragmentHelper;

/**
 * Created by KW on 19/12/2015.
 *
 * Base activity class that implements the View interface.
 */
public abstract class BaseActivity extends AppCompatActivity implements MVP.View {

    private WeakReference<RetainedFragmentHelper> retainedFragmentHelperWeakReference;
    private WeakReference<MVP.Presenter> mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializePresenter();
        initializeViews();

        initializeRetainedFragment(getPresenter());
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void notifyBeingDestroyed() {

    }

    @Override
    public void initializeRetainedFragment(MVP.Presenter presenter) {
        retainedFragmentHelperWeakReference = new WeakReference<>(new RetainedFragmentHelper
                (this.getClass().getSimpleName(), this));
        if (retainedFragmentHelperWeakReference.get().retainedFragmentExists()) {
            retainedFragmentHelperWeakReference.get().putObject(presenter);
        }
        else {
            reinitializeRetainedFragment(presenter);
        }

    }

    @Override
    public void reinitializeRetainedFragment(MVP.Presenter presenter) {
        mPresenter = new WeakReference<>(presenter);
        if (getPresenter()==null)
            initializeRetainedFragment(presenter);
        else {
            getPresenter().notifyConfigChanged(this);
        }
    }

    protected MVP.Presenter getPresenter() {
        return mPresenter.get();
    }

    abstract void initializePresenter();
    abstract void initializeViews();
}
