package kw.kwstudios.simplemvp.base;

import android.content.Context;

import java.lang.ref.WeakReference;

import kw.kwstudios.simplemvp.helper.MVP;

/**
 * Created by KW on 19/12/2015.
 */
public abstract class BasePresenter implements MVP.Presenter {
    private WeakReference<MVP.View> mView;
    private volatile boolean isRunning;

    public BasePresenter(MVP.View v) {
        initializeWeakRef(v);
        initializeModel();
        isRunning = false;
    }

    private void initializeWeakRef(MVP.View v) {
        if (mView == null)
            mView = new WeakReference<>(v);
    }

    protected abstract void initializeModel();

    @Override
    public Context getContext() {
        return getView().getContext();
    }

    @Override
    public boolean isRunning() {
        return isRunning;
    }

    @Override
    public void notifyConfigChanged(MVP.View v) {
        MVP.View newViewRef = v;
        initializeWeakRef(v);
    }

    private MVP.View getView() {
        return mView.get();
    }
}
