package kw.kwstudios.simplemvp.base;

import java.lang.ref.WeakReference;

import kw.kwstudios.simplemvp.helper.MVP;

/**
 * Created by KW on 19/12/2015.
 */
public abstract class BaseModel implements MVP.Model {
    WeakReference<MVP.Presenter> mPresenter;

    public BaseModel(MVP.Presenter presenter) {
        mPresenter = new WeakReference<>(presenter);
    }
}
